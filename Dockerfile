FROM node:12

WORKDIR /app
COPY . .

RUN npm install --no-audit --no-package-lock

ENTRYPOINT ["npm", "run", "start:raw"]
